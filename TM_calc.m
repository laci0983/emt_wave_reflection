function [Etm1_minus,EtmN_plus] = TM_calc(Etm1_plus,lamdba,theta,d,eps_r)
% This function calculates the reflected electric component of a given TM
% wave, depending from the angle of incidence, where m layers are atteched
% to each other
%@param:
%       lambda      -   wavelength of the EM wave : scalar [m]
%       theta       -   angle of incidence        : scalar [radians]
%       d           -   layer locations in z coordinate : 1 x m+1 vector,
%                       where the first element is 0, representing the
%                       first surface [m]
%       eps_r       -   relative permittivity of the layers:1 x m+2 vector,
%                       where  first and last elements are the relative
%                       permittivity of the surrounding substance
%@return:
%       Etm1_minus -  complex amplitude of the reflected component
%       EtmN_plus  -  complex amplitude of the passing component

mu_0=4*pi*1e-7;  %H/m
eps_0=8.85e-12;

eps=eps_0*eps_r;
c=1/sqrt(eps(1)*mu_0);

omega=2*pi*c/(lamdba);


dist_z=cumsum(d);

k_xy=omega*sqrt(mu_0*eps(1))*sin(theta);
k_z=sqrt(omega^2*mu_0*eps-k_xy^2);

Ztm=k_z./(omega*eps);

Y=eye(2);
for n=1:length(eps_r)-1
    a11=exp(-1i*k_z(n)*dist_z(n));
    a12=-exp(+1i*k_z(n)*dist_z(n));
    a21=a11./Ztm(n);
    a22=-a12./Ztm(n);
    
    A=[a11,a12;a21,a22];
    
    b11=exp(-1i*k_z(n+1)*dist_z(n));
    b12=-exp(+1i*k_z(n+1)*dist_z(n));
    b21=b11./Ztm(n+1);
    b22=-b12./Ztm(n+1);
    
    B=[b11,b12;b21,b22];
    
    X=A\B;
    
    Y=X*Y;    
end


Etm1_minus=-Y(2,1)/Y(2,2)*Etm1_plus;
EtmN_plus=(Y(1,1)-Y(1,2)*Y(2,1)/Y(2,2))*Etm1_plus;

end

