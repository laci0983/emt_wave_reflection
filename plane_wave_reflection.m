clear all
close all
clc



%%%%Inilialize parameters
eps_1=1;  %first layer relative permittivity
eps_3=3;   %last layer relative permittivity

lambda=510e-9; % 510 nm
mu_0=4*pi*1e-7;  %H/m




%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Finding the minimum with graphical localization%%%%
%%%%%%%%%Only for 1 inserted layer%%%%%%%%%%%%%%%%%%%%%%%
d = (lambda/420:lambda/420:lambda)*1e9; %[nm]
eps_r2 = 1:0.05:10;
[D,EPS_R2] = meshgrid(d,eps_r2);

refR=zeros(size(D));
for i=1:size(D,1)
    for j=1:size(D,2)
        d_i=D(i,j);
        eps_R2_i=EPS_R2(i,j);
        refR(i,j)=objfun_refl([d_i,eps_R2_i]);
    end
end

figure
surf(D.*1e-9,EPS_R2,refR)
xlabel('distance [m]');
ylabel('Relative permittivity \epsilon');
zlabel('Reflective ratio [%]');
title('Objective function of reflection')



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%% Minimization with global optimization toolbox%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%optimtool('ga') ; %graphical interface for genetic alg
clc

N=1; %Number of layer to be inserted

%0:=genetic algorithm, 1:=particle swarm,
%2:= pattern search , 3:=simulated annealing
selected_method=2; 

%0:=for 510nm EM wave, 1:=for visible light
optimization_way=1; 

%perform optimalization with the selected method
[d_out,eps_2,fval] = minimize_reflection(optimization_way,selected_method,N);

%print out optimal parameters
d_out
eps_2
fval


%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% check for all lightwaves between 380-740 nm %%%%%
%%%%% For the optimal parameters %%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
theta=9*pi/180;
resolution=1e-9; %nm
lambda_min=300e-9;
lambda_max=1200e-9;
refl_ratios=[];
pass_ratios=[];

% eps_2=1.73;

% eps_2=[2.176,1.399];
% d=[0 1469.56e-9 488.057e-9];
d=[0 d_out];
eps_r=[eps_1 eps_2 eps_3];
for lambda_i=lambda_min:resolution:lambda_max
    [refl_ratio,pass_ratio] = calc_reflection(lambda_i,theta,d,eps_r);
    refl_ratios=[refl_ratios,refl_ratio];
    pass_ratios=[pass_ratios,pass_ratio];
end

figure
plot((lambda_min:resolution:lambda_max)*1e9,refl_ratios);
title(['Reflection of visible light. ','width=',num2str(d_out),', epsilon_r=',num2str(eps_2)]);
ylabel('Reflective ratio [%]');
xlabel('\lambda [nm] ')


%Plotting luminosity function
if(optimization_way==1)
    hold on
    hold on; plot(380:1:800,luminosity((380:1:800)*1e-9)*100); 
end



%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%% Plot d,theta,ratio %%%%%%%%
%%%%%%%%%%%% For 1 inserted layer%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
eps_2=1.7318;   % second layer relative permittivity
eps_r=[eps_1 eps_2 eps_3];

d = lambda/25:lambda/10:5*lambda;
theta = (0:0.1:10)*pi/180;
[D,THETA] = meshgrid(d,theta);
reflR=NaN(size(D));
passR=NaN(size(D));
for i=1:size(D,1)
    for j=1:size(D,2)
        d_i=D(i,j);
        theta_i=THETA(i,j);
        [refl_ratio,pass_ratio] = calc_reflection(lambda,theta_i,[0,d_i],eps_r);
        reflR(i,j)=refl_ratio;
        passR(i,j)=pass_ratio;
    end
end
figure
surf(D,THETA,reflR);
xlabel('width [m]');
ylabel('Angle \theta  [rad]');
zlabel('Reflective ratio [%]');
title('Reflection dependence from \theta and layer width')
