function [d_out,eps_2,fval] = minimize_reflection(optimization_way,selected_method,N)
%This function performs search for an optimal layer width and relative
%permittivity, where N layers are attached to each other, and the goal is
%to minimize reflection. The method uses the following minimization
%Methods: 
%          0:=Genetic algorithm
%          1:=Particle swarn optimalization%          
%          2:=Pattern search
%          3:=Simulated annealing
%Goal of optimization:
%          0:=minimize 510nm wavelength reflection
%          1:=minimize visible light reflection
%@param:
%          - optimization_way   {0,1} see Goal of optimization :scalar
%          - selected_method    {0,1,2,3,4} see Methods   :scalar
%          - N                  number of inserted layers :scalar
%@return:
%          - d_out - optimal width of layers : 1xN vector
%          - eps_2 - optimal reletive permittivity of layer : 1xN vector

if(optimization_way==0)
    objfun=@objfun_refl;
elseif(optimization_way==1)
    objfun=@objfun_visible_refl;
end
%   #1    #2     #N/2 #N/2+1   #N
minIntervall=ones(1,N*2);                   % [  1    1   ...  1   1  1 ... 1]
maxIntervall=[500*ones(1,N),3*ones(1,N)]; % [ 3000 3000 ...3000 10 10 ...10]

x0=[200*ones(1,N),2*ones(1,N)]; %d~lambda, eps=2 initially

if(selected_method==0) %genetic algorithm
    [x,fval,~, ~]= ga(objfun,N*2,[],[],[],[], minIntervall,maxIntervall);
    
elseif(selected_method==1) %particle swarn optimalization
    [x,fval,~, ~]=...
        particleswarm(objfun,2*N,ones(1,N*2),[3000*ones(1,N),10*ones(1,N)]);
    
elseif (selected_method==2)%pattern search
    [x,fval,~, ~]=...
        patternsearch(objfun,x0,[],[],[],[], minIntervall,maxIntervall);
elseif(selected_method==3)%simulated annealing
    [x,fval,~, ~]=...
        simulannealbnd(objfun,x0,minIntervall,maxIntervall);
end

%optimal parameters
d_out=x(1:N)*1e-9;
eps_2=x(N+1:2*N);
end

