function [refl_ratio,pass_ratio] = calc_reflection(lambda,theta,d,eps_r)
% This function calculates the reflection ratio and the passage ratio of
% an incident plane wave, where m surfaces are attached to each other, 
%surrounded by a  given substance
%@param:
%       lambda      -   wavelength of the EM wave : scalar [m]
%       theta       -   angle of incidence        : scalar [radians]
%       d           -   layer locations in z coordinate : 1 x m+1 vector,
%                       where the first element is 0, representing the
%                       first surface [m]
%       eps_r       -   relative permittivity of the layers:1 x m+2 vector,
%                       where  first and last elements are the relative
%                       permittivity of the surrounding substance
%@return:
%       reflt_ratio -  ratio of the reflected EM wave intensity : scalar
%       pass_ratio  -  ratio of the passing  EM wave intensity  : scalar

Ete1_p=1;
Etm1_p=1;


[Ete1_minus,EteN_plus]=TE_calc(Ete1_p,lambda,theta,d,eps_r);
[Etm1_minus,EtmN_plus] = TM_calc(Etm1_p,lambda,theta,d,eps_r);

refl_ratio=sqrt(abs(Ete1_minus)^2+abs(Etm1_minus)^2)/...
      sqrt(abs(Ete1_p)^2+abs(Etm1_p)^2)*100;
  
pass_ratio=sqrt(abs(EteN_plus)^2+abs(EtmN_plus)^2)/...
      sqrt(abs(Ete1_p)^2+abs(Etm1_p)^2)*100;
end

