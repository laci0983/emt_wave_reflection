function reflR = objfun_visible_refl(params)
% This function used used as an objective function in order to minimize the
% reflection of an EM wave with 510nm wavelength. The relative
%permittivity of the first layer is 1, while the relative permittivity of
%the last layer is 3. The goal is to minimize the reflection by
%determining the corresponding parameters for the minimum
%@param:
%       params = [d eps_layer]
%               -> d  - width parameters of the layers : 1xm vector [nm]
%               -> eps_layer - relative permittivity of the layers : 1xm
%                                                             vector
%@return:
%       ref1R - objective function value in [0 100] intervall, where the
%               reflection at smaller angles of incidence are of higher
%               significance, decreasing proportionally until 10� of
%               incidance where the weight is the lowest

d=params(1:end/2)*1e-9;
eps_layer=params(end/2+1:end);
if(d<=0 | eps_layer<=1)
    reflR=inf;
    return;
end

%lambda=510e-9; % 510 nm

eps_1=1;  %initial relative permittivity
eps_last=3;   %last layer relative permittivity

eps_r=[eps_1 eps_layer eps_last];



%%%% Weight function for luminosity%%%%%%
resolution_lambda=10; %nm
lambda_min=380;
lambda_max=740;
lambda=lambda_min:resolution_lambda:lambda_max;
W_luminosity=luminosity(lambda*1e-9);

%%%% Weight function for angle of incidence%%%%%%
resolution_theta=0.1;
theta=(0:resolution_theta:10)*pi/180;
W_theta=trimf(theta,[0,0,20*pi/180]);
W_theta=W_theta/(sum(W_theta));


reflR=0;


for k=1:length(theta)
        theta_k=theta(k);
        W_theta_k=W_theta(k);
    for i=1:length(lambda) %calculate for visible light
        lambda_i=lambda(i);
        [refl_ratio,~] = calc_reflection(lambda_i*1e-9,theta_k,[0,d],eps_r);
        weight=(W_theta_k*W_luminosity(i))+(refl_ratio>10)*(refl_ratio-10)/90;
        reflR=reflR+weight*refl_ratio;
    end
end
end


